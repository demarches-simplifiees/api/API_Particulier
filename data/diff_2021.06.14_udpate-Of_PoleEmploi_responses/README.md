# 2021.06.14 - Update of PoleEmploi responses

> Changement des données renvoyées par PE.
>
> Le champ `categorieInscription`, auparavant textuel, est maintenant splitté en deux champs :
> `codeCategorieInscription`, chiffre et `libelleCategorieInscription`, texte.

## Diff

```diff
   "dateInscription": "1965-05-03T00:00:00.000Z",
   "dateCessationInscription": "1966-05-03T00:00:00.000Z",
-  "categorieInscription": "PERSONNE SANS EMPLOI DISPONIBLE DUREE INDETERMINEE PLEIN TPS"
+  "codeCategorieInscription": 1,
+  "libelleCategorieInscription": "PERSONNE SANS EMPLOI DISPONIBLE DUREE INDETERMINEE PLEIN TPS"
 }
```

## Before

```json
{
  "identifiant": "georges_moustaki_77",
  "civilite": "M.",
  "nom": "Moustaki",
  "nomUsage": "Moustaki",
  "prenom": "Georges",
  "sexe": "M",
  "dateNaissance": "1934-05-03T00:00:00.000Z",
  "codeCertificationCNAV": "VC",
  "telephone": "0629212921",
  "email": "georges@moustaki.fr",
  "adresse": {
    "codePostal": "75018",
    "INSEECommune": "75118",
    "localite": "75018 Paris",
    "ligneVoie": "3 rue des Huttes",
    "ligneNom": "MOUSTAKI"
  },
  "dateInscription": "1965-05-03T00:00:00.000Z",
  "dateCessationInscription": "1966-05-03T00:00:00.000Z",
  "categorieInscription": "PERSONNE SANS EMPLOI DISPONIBLE DUREE INDETERMINEE PLEIN TPS"
}
```

## After

```json
{
  "identifiant": "georges_moustaki_77",
  "civilite": "M.",
  "nom": "Moustaki",
  "nomUsage": "Moustaki",
  "prenom": "Georges",
  "sexe": "M",
  "dateNaissance": "1934-05-03T00:00:00.000Z",
  "codeCertificationCNAV": "VC",
  "telephone": "0629212921",
  "email": "georges@moustaki.fr",
  "adresse": {
    "codePostal": "75018",
    "INSEECommune": "75118",
    "localite": "75018 Paris",
    "ligneVoie": "3 rue des Huttes",
    "ligneNom": "MOUSTAKI"
  },
  "dateInscription": "1965-05-03T00:00:00.000Z",
  "dateCessationInscription": "1966-05-03T00:00:00.000Z",
  "codeCategorieInscription": 1,
  "libelleCategorieInscription": "PERSONNE SANS EMPLOI DISPONIBLE DUREE INDETERMINEE PLEIN TPS"
}
```
