# API Particluer data

## Test datas

see: [api.gouv.fr/documentation/api-particulier](https://api.gouv.fr/documentation/api-particulier)

[All **test data** downloaded on 01/05/2021](2021.05.21/)


## Test tokens

see: [api.gouv.fr/documentation/api-particulier](https://api.gouv.fr/documentation/api-particulier)

| Token                                  | Scopes IDs                                                                       | Scopes                                                      |
|----------------------------------------|----------------------------------------------------------------------------------| ------------------------------------------------------------|
| `83c68bf0b6013c4daf3f8213f7212aa5`     | `dgfip_avis_imposition`                                                          | DGFIP - Avis d'imposition                                   |
| `02013fe1b5221dd7d914e4406fb88891`     | `dgfip_adresse`                                                                  | DGFIP - Adresse                                             |
| `73af98c480aa3abfd38830ec5c5354d8`     | `cnaf_quotient_familial`                                                         | CNAF - Quotient familial                                    |
| `3841b13fa8032ed3c31d160d3437a76a`     | `cnaf_allocataires` + `cnaf_enfants`  + `cnaf_adresse`                           | CNAF - Situation familiale                                  |
| `e0a109ba-8e2b-4809-95c5-6a818dda2351` | `cnaf_adresse`                                                                   | CNAF - Adresse                                              |
| `ce469960-7bfc-401d-8599-686d158939ae` | `cnaf_enfants`                                                                   | CNAF - Enfants                                              |
| `ea903339-692b-4cc2-96b3-09a2d6acd4d2` | `cnaf_allocataires`                                                              | CNAF - Allocataires                                         |
| `d7e9c9f4c3ca00caadde31f50fd4521a`     | `dgfip_avis_imposition` + `cnaf_allocataires` + `cnaf_enfants`  + `cnaf_adresse` | "DGFIP - Avis d'imposition" et "CNAF - Situation familiale" |
| `fd38830c480221d0ff0b6013c4de6c32`     | `pole_emploi_identite`  + `pole_emploi_contact` + `pole_emploi_adresse` + `pole_emploi_inscription`  | Pôle Emploi - Identité, Contact, Adresse et Inscription |
| `1b8bea9a1df409af64c995e58014f642`     | `mesri_statut_etudiant`                                                          | Statut Étudiant                                             |


## `api/introspect` endpoint

```json
{
  "_id": "...",
  "name": "Application de sandbox",
  "scopes": [
    "dgfip_avis_imposition",
    "dgfip_adresse",
    "cnaf_allocataires",
    "cnaf_enfants",
    "cnaf_adresse",
    "cnaf_quotient_familial",
    "pole_emploi_identite",
    "pole_emploi_contact",
    "pole_emploi_adresse",
    "pole_emploi_inscription",
    "mesri_statut_etudiant"
  ]
}
```

### CURL queries of all test tokens

```bash
# DGFIP - Avis d'imposition    83c68bf0b6013c4daf3f8213f7212aa5
curl     -X GET "https://particulier-test.api.gouv.fr/api/introspect" \
    -H  "accept: application/json" \
    -H  "X-API-Key: 83c68bf0b6013c4daf3f8213f7212aa5" | jq

# DGFIP - Adresse    02013fe1b5221dd7d914e4406fb88891
curl     -X GET "https://particulier-test.api.gouv.fr/api/introspect" \
    -H  "accept: application/json" \
    -H  "X-API-Key: 02013fe1b5221dd7d914e4406fb88891" | jq

# CNAF - Quotient familial    73af98c480aa3abfd38830ec5c5354d8
curl     -X GET "https://particulier-test.api.gouv.fr/api/introspect" \
    -H  "accept: application/json" \
    -H  "X-API-Key: 73af98c480aa3abfd38830ec5c5354d8" | jq

# CNAF - Situation familiale    3841b13fa8032ed3c31d160d3437a76a
curl     -X GET "https://particulier-test.api.gouv.fr/api/introspect" \
    -H  "accept: application/json" \
    -H  "X-API-Key: 3841b13fa8032ed3c31d160d3437a76a" | jq

# CNAF - Adresse    e0a109ba-8e2b-4809-95c5-6a818dda2351
curl     -X GET "https://particulier-test.api.gouv.fr/api/introspect" \
    -H  "accept: application/json" \
    -H  "X-API-Key: e0a109ba-8e2b-4809-95c5-6a818dda2351" | jq

# CNAF - Enfants    ce469960-7bfc-401d-8599-686d158939ae
curl     -X GET "https://particulier-test.api.gouv.fr/api/introspect" \
    -H  "accept: application/json" \
    -H  "X-API-Key: ce469960-7bfc-401d-8599-686d158939ae" | jq

# CNAF - Allocataires    ea903339-692b-4cc2-96b3-09a2d6acd4d2
curl     -X GET "https://particulier-test.api.gouv.fr/api/introspect" \
    -H  "accept: application/json" \
    -H  "X-API-Key: ea903339-692b-4cc2-96b3-09a2d6acd4d2" | jq

# "DGFIP - Avis d'imposition" et "CNAF - Situation familiale"    d7e9c9f4c3ca00caadde31f50fd4521a
curl     -X GET "https://particulier-test.api.gouv.fr/api/introspect" \
    -H  "accept: application/json" \
    -H  "X-API-Key: d7e9c9f4c3ca00caadde31f50fd4521a" | jq

# Pôle Emploi - Identité, Contact, Adresse et Inscription     fd38830c480221d0ff0b6013c4de6c32
curl     -X GET "https://particulier-test.api.gouv.fr/api/introspect" \
    -H  "accept: application/json" \
    -H  "X-API-Key: fd38830c480221d0ff0b6013c4de6c32" | jq

# Statut Étudiant    1b8bea9a1df409af64c995e58014f642
curl     -X GET "https://particulier-test.api.gouv.fr/api/introspect" \
    -H  "accept: application/json" \
    -H  "X-API-Key: 1b8bea9a1df409af64c995e58014f642" | jq

# Clé d'API invalide
curl     -X GET "https://particulier-test.api.gouv.fr/api/introspect" \
    -H  "accept: application/json" \
    -H "X-API-Key: fakeToken" | jq

# Clé d'API non présente
curl     -X GET "https://particulier-test.api.gouv.fr/api/introspect" \
    -H  "accept: application/json" | jq
```


