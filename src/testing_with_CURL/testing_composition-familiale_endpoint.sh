#!/bin/bash

set -o errexit
##########################################
PARAM_NUMERO_ALLOCATAIRE="0000001"
PARAM_CODE_POSTAL="75001"
API_URL="https://particulier-test.api.gouv.fr/api/v2"
API_ENDPOINT="composition-familiale"
API_ENDPOINT_URL="${API_URL}/${API_ENDPOINT}?numeroAllocataire=${PARAM_NUMERO_ALLOCATAIRE}&codePostal=${PARAM_CODE_POSTAL}"
BUILD_DIR="../../build/CURL_results/${API_ENDPOINT}_endpoint"
##########################################
if [ -d "${BUILD_DIR}" ]; then
    rm -rf ${BUILD_DIR}
fi
mkdir -p "${BUILD_DIR}"
touch "${BUILD_DIR}/checksum.sha256"
##########################################

# DGFIP - Avis d'imposition    83c68bf0b6013c4daf3f8213f7212aa5
API_TOKEN="83c68bf0b6013c4daf3f8213f7212aa5"
RESULT_FILE="${BUILD_DIR}/${API_ENDPOINT}_token-${API_TOKEN}.json"
curl -X GET "${API_ENDPOINT_URL}"   \
     -H  "accept: application/json" \
     -H  "X-API-Key: ${API_TOKEN}" > "${RESULT_FILE}"
sha256sum "${RESULT_FILE}" >> "${BUILD_DIR}/checksum.sha256"

# DGFIP - Adresse    02013fe1b5221dd7d914e4406fb88891
API_TOKEN="02013fe1b5221dd7d914e4406fb88891"
RESULT_FILE="${BUILD_DIR}/${API_ENDPOINT}_token-${API_TOKEN}.json"
curl -X GET "${API_ENDPOINT_URL}"   \
     -H  "accept: application/json" \
     -H  "X-API-Key: ${API_TOKEN}" > "${RESULT_FILE}"
sha256sum "${RESULT_FILE}" >> "${BUILD_DIR}/checksum.sha256"

# CNAF - Quotient familial    73af98c480aa3abfd38830ec5c5354d8
API_TOKEN="73af98c480aa3abfd38830ec5c5354d8"
RESULT_FILE="${BUILD_DIR}/${API_ENDPOINT}_token-${API_TOKEN}.json"
curl -X GET "${API_ENDPOINT_URL}"   \
     -H  "accept: application/json" \
     -H  "X-API-Key: ${API_TOKEN}" > "${RESULT_FILE}"
sha256sum "${RESULT_FILE}" >> "${BUILD_DIR}/checksum.sha256"

# CNAF - Situation familiale    3841b13fa8032ed3c31d160d3437a76a
API_TOKEN="3841b13fa8032ed3c31d160d3437a76a"
RESULT_FILE="${BUILD_DIR}/${API_ENDPOINT}_token-${API_TOKEN}.json"
curl -X GET "${API_ENDPOINT_URL}"   \
     -H  "accept: application/json" \
     -H  "X-API-Key: ${API_TOKEN}" > "${RESULT_FILE}"
sha256sum "${RESULT_FILE}" >> "${BUILD_DIR}/checksum.sha256"

# CNAF - Adresse    e0a109ba-8e2b-4809-95c5-6a818dda2351
API_TOKEN="e0a109ba-8e2b-4809-95c5-6a818dda2351"
RESULT_FILE="${BUILD_DIR}/${API_ENDPOINT}_token-${API_TOKEN}.json"
curl -X GET "${API_ENDPOINT_URL}"   \
     -H  "accept: application/json" \
     -H  "X-API-Key: ${API_TOKEN}" > "${RESULT_FILE}"
sha256sum "${RESULT_FILE}" >> "${BUILD_DIR}/checksum.sha256"

# CNAF - Enfants    ce469960-7bfc-401d-8599-686d158939ae
API_TOKEN="ce469960-7bfc-401d-8599-686d158939ae"
RESULT_FILE="${BUILD_DIR}/${API_ENDPOINT}_token-${API_TOKEN}.json"
curl -X GET "${API_ENDPOINT_URL}"   \
     -H  "accept: application/json" \
     -H  "X-API-Key: ${API_TOKEN}" > "${RESULT_FILE}"
sha256sum "${RESULT_FILE}" >> "${BUILD_DIR}/checksum.sha256"

# CNAF - Allocataires    ea903339-692b-4cc2-96b3-09a2d6acd4d2
API_TOKEN="ea903339-692b-4cc2-96b3-09a2d6acd4d2"
RESULT_FILE="${BUILD_DIR}/${API_ENDPOINT}_token-${API_TOKEN}.json"
curl -X GET "${API_ENDPOINT_URL}"   \
     -H  "accept: application/json" \
     -H  "X-API-Key: ${API_TOKEN}" > "${RESULT_FILE}"
sha256sum "${RESULT_FILE}" >> "${BUILD_DIR}/checksum.sha256"

# "DGFIP - Avis d'imposition" et "CNAF - Situation familiale"    d7e9c9f4c3ca00caadde31f50fd4521a
API_TOKEN="d7e9c9f4c3ca00caadde31f50fd4521a"
RESULT_FILE="${BUILD_DIR}/${API_ENDPOINT}_token-${API_TOKEN}.json"
curl -X GET "${API_ENDPOINT_URL}"   \
     -H  "accept: application/json" \
     -H  "X-API-Key: ${API_TOKEN}" > "${RESULT_FILE}"
sha256sum "${RESULT_FILE}" >> "${BUILD_DIR}/checksum.sha256"

# Pôle Emploi - Identité, Contact, Adresse et Inscription    fd38830c480221d0ff0b6013c4de6c32
API_TOKEN="fd38830c480221d0ff0b6013c4de6c32"
RESULT_FILE="${BUILD_DIR}/${API_ENDPOINT}_token-${API_TOKEN}.json"
curl -X GET "${API_ENDPOINT_URL}"   \
     -H  "accept: application/json" \
     -H  "X-API-Key: ${API_TOKEN}" > "${RESULT_FILE}"
sha256sum "${RESULT_FILE}" >> "${BUILD_DIR}/checksum.sha256"

# Pôle Emploi - Identité    d76d8c36-f49d-4c26-b849-7466a9faf0d6
API_TOKEN="d76d8c36-f49d-4c26-b849-7466a9faf0d6"
RESULT_FILE="${BUILD_DIR}/${API_ENDPOINT}_token-${API_TOKEN}.json"
curl -X GET "${API_ENDPOINT_URL}"   \
     -H  "accept: application/json" \
     -H  "X-API-Key: ${API_TOKEN}" > "${RESULT_FILE}"
sha256sum "${RESULT_FILE}" >> "${BUILD_DIR}/checksum.sha256"

# Pôle Emploi - Contact   b70015d5-6ecd-48a6-9a2b-9fdb9fdf1c67
API_TOKEN="b70015d5-6ecd-48a6-9a2b-9fdb9fdf1c67"
RESULT_FILE="${BUILD_DIR}/${API_ENDPOINT}_token-${API_TOKEN}.json"
curl -X GET "${API_ENDPOINT_URL}"   \
     -H  "accept: application/json" \
     -H  "X-API-Key: ${API_TOKEN}" > "${RESULT_FILE}"
sha256sum "${RESULT_FILE}" >> "${BUILD_DIR}/checksum.sha256"

# Pôle Emploi - Adresse    438e839a-da85-4e10-a314-98092b856989
API_TOKEN="438e839a-da85-4e10-a314-98092b856989"
RESULT_FILE="${BUILD_DIR}/${API_ENDPOINT}_token-${API_TOKEN}.json"
curl -X GET "${API_ENDPOINT_URL}"   \
     -H  "accept: application/json" \
     -H  "X-API-Key: ${API_TOKEN}" > "${RESULT_FILE}"
sha256sum "${RESULT_FILE}" >> "${BUILD_DIR}/checksum.sha256"

# Pôle Emploi - Inscription    d726502d-ac6d-483f-b8b8-c9ba239547a6
API_TOKEN="d726502d-ac6d-483f-b8b8-c9ba239547a6"
RESULT_FILE="${BUILD_DIR}/${API_ENDPOINT}_token-${API_TOKEN}.json"
curl -X GET "${API_ENDPOINT_URL}"   \
     -H  "accept: application/json" \
     -H  "X-API-Key: ${API_TOKEN}" > "${RESULT_FILE}"
sha256sum "${RESULT_FILE}" >> "${BUILD_DIR}/checksum.sha256"

# Statut Étudiant    1b8bea9a1df409af64c995e58014f642
API_TOKEN="1b8bea9a1df409af64c995e58014f642"
RESULT_FILE="${BUILD_DIR}/${API_ENDPOINT}_token-${API_TOKEN}.json"
curl -X GET "${API_ENDPOINT_URL}"   \
     -H  "accept: application/json" \
     -H  "X-API-Key: ${API_TOKEN}" > "${RESULT_FILE}"
sha256sum "${RESULT_FILE}" >> "${BUILD_DIR}/checksum.sha256"

# Invalid API key
API_TOKEN="fakeToken"
RESULT_FILE="${BUILD_DIR}/${API_ENDPOINT}_token-${API_TOKEN}.json"
curl -X GET "${API_ENDPOINT_URL}"   \
     -H  "accept: application/json" \
     -H  "X-API-Key: ${API_TOKEN}" > "${RESULT_FILE}"
sha256sum "${RESULT_FILE}" >> "${BUILD_DIR}/checksum.sha256"

# API key missing
RESULT_FILE="${BUILD_DIR}/${API_ENDPOINT}_token-missing_API_TOKEN.json"
curl -X GET "${API_ENDPOINT_URL}"   \
     -H  "accept: application/json" \
     -H  "X-API-Key: ${API_TOKEN}" > "${RESULT_FILE}"
sha256sum "${RESULT_FILE}" >> "${BUILD_DIR}/checksum.sha256"

##########################################
cat "${BUILD_DIR}/checksum.sha256"
